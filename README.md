# Design of Jnktn

Some general information about the design of Jntkn.

# General colors

DESC | HEX | RGB | CMYK | HSL
---|---|---|---|---
Jnktn Yellow | `#e5cf18` | rgb(229, 207, 24) | cmyk(0,10,90,10) | hsl(53.6,81%,49.6%)
Jnktn Black | `#000000` | rgb(0,0,0) | cmyk(NaN,NaN,NaN,100) | hsl(0,0%,0%)
Pref Light Gray Tone | `#e5e5e5` | rgb(229,229,229) | cmyk(0,0,0,10) | hsl(0,0%,90.2%)

# Resolutions

SD = 1021x1304 px / 300 ppi (27% of HQ)

HQ = 3780x4830 px / 300 ppi

# Naming scheme

`tag_date_theme_quality.file-ext` > `pg_20220505_we-are-back_hq.png`

Tag:
- pg = poster general
- ps = poster schedule

Date:
- ISO format

Quality:
- hq = High Quality
- sd = standard definition

# LICENSE

WIP

--------

TODO:
- templates-
- logos
- licensing information
